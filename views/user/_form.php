<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'rememberMe')->checkbox() ?>
	


  
    <div class="form-group">
		<div class="col-lg-offset-1 col-lg-11">
        <?= Html::submitButton('Login',
		['class'=> 'btn btn-primary',
		'name' => 'login-button']) ?>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>