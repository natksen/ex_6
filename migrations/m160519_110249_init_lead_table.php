<?php

use yii\db\Migration;

class m160519_110249_init_lead_table extends Migration
{
    public function up()
    {
        $this->createTable(
            'lead',
            [
                'id' => 'pk',
                'name' => 'string',	
                'notes' => 'text',
				'status' => 'integer',
				'birth_date' => 'date',
            ],
            'ENGINE=InnoDB'
        );

    }

    public function down()
    {
        $this->dropTable('lead');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}